package PT2018.demo.Polinoame;

public abstract class Monom {
	protected int grad;
    
	public abstract int getGrad();
	public abstract Number getCoef();
	public abstract String toString();
	public abstract Monom integreaza();
	public abstract void deriveaza();
	public abstract Monom adunare(Monom M);
	public abstract Monom scadere(Monom M);
	public abstract boolean isInt();
	public abstract  Monom inmultire(Monom mon);
	public abstract Monom auxImpartire(Monom M2);
}