package PT2018.demo.Polinoame;

public class MonomReal extends Monom {
	
	private double coef;
	
	public MonomReal(double d,int g){
		this.grad=g;
		this.coef=d;
	}
	
	public MonomReal(){
		this.grad=0;
		this.coef=0d;
	}
	
	@Override
	public Double getCoef() {
		return coef;
	}
	
	public int getGrad(){
		return grad;
	}
	@Override
	public Monom adunare(Monom M) {
		Monom m=new MonomReal(this.coef+M.getCoef().doubleValue(),this.grad);
		return m;
	}
	


	@Override
	public Monom scadere(Monom M) {
		Monom rez=new MonomReal(this.coef-M.getCoef().doubleValue(),this.grad);
		return rez;
	}
	
			
	public Monom integreaza(){
		double rezCoef=this.coef/(this.grad+1);
		int rezGrad=this.grad+1;
		Monom rez=new MonomReal(rezCoef,rezGrad);
		return rez;
	}
	
	public void deriveaza(){
		this.coef*=this.grad;
		this.grad-=1;
	}
	
	public Monom inmultire(Monom mon){
		Monom auxM=new MonomReal(this.coef*mon.getCoef().doubleValue(),this.grad+mon.getGrad());
		return auxM;
	}
	
	
	public Monom auxImpartire(Monom M2){	
		double rest1=this.getCoef().doubleValue() / M2.getCoef().doubleValue();
		int restG=this.getGrad()-M2.getGrad();
		Monom m=new MonomReal(rest1,restG);
		return m;
}	
	
	public static String superscript(int s) {
		String str=""+s;
	    str = str.replaceAll("0", "⁰");
	    str = str.replaceAll("1", "¹");
	    str = str.replaceAll("2", "²");
	    str = str.replaceAll("3", "³");
	    str = str.replaceAll("4", "⁴");
	    str = str.replaceAll("5", "⁵");
	    str = str.replaceAll("6", "⁶");
	    str = str.replaceAll("7", "⁷");
	    str = str.replaceAll("8", "⁸");
	    str = str.replaceAll("9", "⁹");         
	    return str;
	}
	
	
	@Override
	public String toString() {
		String string="";
		if(coef==1){
			if (this.grad>0){
			string = "x"+superscript(this.getGrad());
			}
			else if(grad==0){
				string+="" +String.format("%.2f",coef);
			}
			else{
				string="";
			}
					}
		else{
			if (this.grad>0){
		
		string = String.format("%.2f",coef) + "x"+superscript(this.getGrad());
		}
		else{
			string="";
		}
		}
		
		return string;
	}

	public boolean isInt(){
		if ((int)this.coef==this.coef){
			return true;
		}
		else{
			return false;
		}
	}
	
}
