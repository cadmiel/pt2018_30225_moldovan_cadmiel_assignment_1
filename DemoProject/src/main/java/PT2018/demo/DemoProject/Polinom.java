package PT2018.demo.DemoProject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Polinom {
	
	private List<Monom> pol;
	private String rest="";
	
	
	public Polinom(){
		this.pol=new ArrayList<Monom>();
	}
	
	public Polinom(List<Monom> lst){
		this.pol=lst;
	}
	
	public List<Monom> getList(){
		return pol;
	}
	
	public void setRest(String s){
		this.rest=s;
	}
	
	private void clean(){
		for (int i=0;i<pol.size();i++){
			if (pol.get(i).getCoef().doubleValue()==0){
				pol.remove(i);
				i-=1;
			}
		}
		Polinom Q=new Polinom();
		for (int i=0;i<pol.size();i++){
			if(pol.get(i).isInt()){
				Monom m=new MonomInt(this.pol.get(i).getCoef().intValue(),this.pol.get(i).getGrad());
				Q.add(m);
			}
			else{
				Q.add(pol.get(i));
			}
		}
		this.pol=Q.getList();
		
	}
	
	
	public void add(Monom M){
		
		this.pol.add(M);
	}
	
	public void ordonare(){
		Collections.sort(this.pol,new Comparator<Monom>(){
			public int compare(Monom m1,Monom m2){
				return m2.getGrad()-m1.getGrad();
			}
		});
	}
		

	public void integreaza(){
		Polinom Q=new Polinom();
		
			while(this.pol.size()>0){
				Monom m=new MonomReal(this.pol.get(0).getCoef().doubleValue(),this.pol.get(0).getGrad());
				pol.remove(0);
				Q.add(m.integreaza());
		}
		Q.clean();
		this.pol=Q.getList();	
	}
		
	public void deriveaza(){
		for (int i=0;i<this.pol.size();i++){
			this.pol.get(i).deriveaza();
		}
		this.clean();
	}
	
	
	public Monom max(){
		int max=-1;
		Monom m=new MonomReal();
		for (int i=0;i<pol.size();i++){
			if (pol.get(i).getGrad()>max){
				max=pol.get(i).getGrad();
				m=pol.get(i);
			}
		}
		return m;
	}
		
	public Polinom adunare(Polinom P){
		Polinom p=new Polinom();
		for (int i=0;i<pol.size();i++){
			for (int j=0;j<P.getList().size();j++){
				if (pol.get(i).getGrad()==P.getList().get(j).getGrad()){
					p.add(pol.get(i).adunare(P.getList().get(j)));
				}
			}
		}
	for(int j=0;j<P.getList().size();j++){
		boolean ok=true;
		for(int k=0;k<p.getList().size();k++){
			if (P.getList().get(j).getGrad()==p.getList().get(k).getGrad()){
				ok=false;
			}	
		}
		if (ok){
			p.add(P.getList().get(j));
		}
	}
	
	for(int j=0;j<pol.size();j++){
		boolean ok=true;
		for(int k=0;k<p.getList().size();k++){
			if (pol.get(j).getGrad()==p.getList().get(k).getGrad()){
				ok=false;
			}	
		}
		if (ok){
			p.add(pol.get(j));
		}
	}
	return p;
	}
	
	public Polinom scadere(Polinom P){
		Polinom p=new Polinom();
		for (int i=0;i<pol.size();i++){
			boolean oo=false;
			int jj=0;
			for (int j=0;j<P.getList().size();j++){
				if (pol.get(i).getGrad()==P.getList().get(j).getGrad()){
					oo=true;
					jj=j;
				}	
			}
			if (oo){
			p.add(pol.get(i).scadere(P.getList().get(jj)));
			}
			else{
				p.add(pol.get(i));
			}	
		}
	for(int j=0;j<P.getList().size();j++){
		boolean ok=true;
		for(int k=0;k<p.getList().size();k++){
			if (P.getList().get(j).getGrad()==p.getList().get(k).getGrad()){
				ok=false;
			}	
		}
		if (ok){
			MonomInt m=new MonomInt(P.getList().get(j).getCoef().intValue()*-1,P.getList().get(j).getGrad());
			p.add(m);
		}
	}
	p.clean();
	return p;
}
	
	public Polinom auxInmultire(Polinom P,Monom M){
		Polinom Q=new Polinom();
		for (int i=0;i<P.getList().size();i++){
			Q.add(P.getList().get(i).inmultire(M));
		}
		Q.clean();
		return Q;
	}
	
	
	public Polinom inmultire(Polinom P2){
		Polinom P=new Polinom();
		for (int i=0;i<this.pol.size();i++){
			P=P.adunare(auxInmultire(P2,this.pol.get(i)));
			
		}
		P.clean();
		return P;
	}
	
	public boolean maiMare(Polinom P1,Polinom P2){
		if(P1.max().getGrad()<P2.max().getGrad()){
			return false;
		}
		if(P1.max().getGrad()==P2.max().getGrad()){
			if(Math.abs(P1.max().getCoef().doubleValue())<Math.abs(P2.max().getCoef().doubleValue())){
				return false;
			}
		}
		return true;
	}
	
	public Polinom impartire(Polinom impartitor){	
		Polinom auxImp=this;
		Polinom rezImpartire=new Polinom();
		while (this.maiMare(auxImp, impartitor)){
			Monom cat=auxImp.max().auxImpartire(impartitor.max());
			rezImpartire.add(cat);
			auxImp=auxImp.scadere(this.auxInmultire(impartitor,cat));
			//auxImp.clean();
			
		}
		rezImpartire.clean();
		if(auxImp.getList().size()!=0){
		rezImpartire.setRest(auxImp.toString());
		}
		return rezImpartire;
	}

	

	
	public String toString(){                         
		this.ordonare();
		String S="";
		for (int i=0;i<pol.size();i++){
			if (pol.get(i).getCoef().doubleValue()<0){
				S+=pol.get(i).toString();
			}
			else{
				S+="+"+pol.get(i).toString();
			}	
		}
		if(S.startsWith("+")){
			S=S.substring(1);
		}
		if(S==""){
			S+="0";
		}
		S+="=0";
	
		if(this.rest!=""){
			S+="   rest="+this.rest;
		}
		return S;
	}
}

