package PT2018.demo.DemoProject;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;




import javax.swing.JLabel;


public class MainForm extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPanel;
	private JTextArea textArea,textArea2,textArea_1,textArea_2,textArea_3,textArea_4,textArea_5,textArea_6;
	private JTextField textField, textField_0,textField_1,textField_2,textField_3,textField_4;
	private Polinom P1, P2;
	private JButton btnAdaugaMonom,  btnAdaugaMonom2, btnAdd,  btnscad, btnInmultire, btnPp,btnDeriveaza, btnDeriveaza_1, btnIntegreaza,  btnIntegreaza_1;
	private JLabel lblCoeficient, lblPutere, lblCoeficient2, lblPutere2, lblPolinom, lblPolinom_1;

	public void main()
	{
		try {
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			this.setVisible(true);
			
		} catch (Exception e) {
		}
	}

	public MainForm() {
		
		setTitle("Polinom");
		Font fnt = new Font("", Font.BOLD, 14);
		contentPanel= new JPanel();
		setBounds(100, 100, 800, 300);//setBounds(int x, int y, int width, int height)
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		
		this.textArea= new JTextArea();
		textArea.setBounds(25, 30, 250, 20);
		textArea.setEditable(false); 
		contentPanel.add(textArea);
		textArea.setFont(fnt);
			
	    P1=new Polinom();
	    P2=new Polinom();
		btnAdaugaMonom = new JButton("Adauga monom1");
		btnAdaugaMonom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addMonom1();
				Display();
			}
		});
		btnAdaugaMonom.setBounds(300, 30, 130, 20);
		contentPanel.add(btnAdaugaMonom);		
		
		this.textField_1=new JTextField();
		this.textField_1.setBounds(360, 60, 50, 20);
		contentPanel.add(this.textField_1);
		this.textField_1.setColumns(10);
		
		this.textField_0=new JTextField();
		this.textField_0.setBounds(300, 60, 50, 20);
		contentPanel.add(this.textField_0);
		this.textField_0.setColumns(10);
		
		
	    lblCoeficient = new JLabel("Coeficient");
		lblCoeficient.setBounds(300, 90, 70, 15);
		contentPanel.add(lblCoeficient);
		
	    lblPutere = new JLabel("Putere");
		lblPutere.setBounds(370, 90, 50, 15);
		contentPanel.add(lblPutere);
		//---------------------------------------------------------------------

		this.textArea2= new JTextArea();
		textArea2.setBounds(25, 120, 250, 20);
		textArea2.setEditable(false); 
		contentPanel.add(textArea2);
		textArea2.setFont(fnt);
	    btnAdaugaMonom2 = new JButton("Adauga monom2");
		btnAdaugaMonom2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addMonom2();
				Display();
			}
		});
		btnAdaugaMonom2.setBounds(300, 120, 130, 20);
		contentPanel.add(btnAdaugaMonom2);		
		
		textField_3=new JTextField();
		textField_3.setBounds(360, 150, 50, 20);
		contentPanel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_2=new JTextField();
		textField_2.setBounds(300, 150,50
				, 20);
		contentPanel.add(textField_2);
		textField_2.setColumns(10);
		
		
	    lblCoeficient2 = new JLabel("Coeficient");
		lblCoeficient2.setBounds(300, 180, 70, 15);
		contentPanel.add(lblCoeficient2);
		
	    lblPutere2 = new JLabel("Putere");
		lblPutere2.setBounds(370, 180, 50, 15);
		contentPanel.add(lblPutere2);
		
		
	    btnAdd = new JButton("P1+P2");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				aduna();
			}
		});
		btnAdd.setBounds(460, 30, 100, 20);
		contentPanel.add(btnAdd);
		
		this.textArea_1 = new JTextArea();
		textArea_1.setBounds(570, 30, 200, 20);
		textArea_1.setEditable(false); 
		contentPanel.add(textArea_1);
		textArea_1.setFont(fnt);
	  
		lblPolinom = new JLabel("Polinom 1");
		lblPolinom.setBounds(25, 10, 100, 14);
		contentPanel.add(lblPolinom);
		
		lblPolinom_1 = new JLabel("Polinom 2");
		lblPolinom_1.setBounds(25, 100, 100, 14);
		contentPanel.add(lblPolinom_1);
		
	    btnscad = new JButton("P1-P2");
		btnscad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				scade();
			}
		});
		btnscad.setBounds(460, 60, 100, 20);
		contentPanel.add(btnscad);
		
		this.textArea_2 = new JTextArea();
		textArea_2.setBounds(570, 60, 200, 20);
		textArea_2.setEditable(false); 
		contentPanel.add(textArea_2);
		textArea_2.setFont(fnt);
		
		btnInmultire = new JButton("Inmultire");
		btnInmultire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inmultire();
			}
		});
		btnInmultire.setBounds(460, 90, 100, 20);
		contentPanel.add(btnInmultire);
		
		this.textArea_3 = new JTextArea();
		textArea_3.setBounds(570, 90, 200, 20);
		textArea_3.setEditable(false); 
		contentPanel.add(textArea_3);
		textArea_3.setFont(fnt);
		
		btnPp = new JButton("Impartire");
		btnPp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				impartire();
			}
		});
		btnPp.setBounds(460, 120, 100, 20);
		contentPanel.add(btnPp);
		
	    btnDeriveaza = new JButton("Deriveaza 1");
		btnDeriveaza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deriveazaP1();
				Display();
			}
		});
		btnDeriveaza.setBounds(25, 61,110, 23);//
		contentPanel.add(btnDeriveaza);
		
		btnDeriveaza_1 = new JButton("Deriveaza 2");
		btnDeriveaza_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deriveazaP2();
				Display();
			}
		});
		btnDeriveaza_1.setBounds(25, 151,110, 23);
		contentPanel.add(btnDeriveaza_1);
		
		btnIntegreaza = new JButton("Integreaza 1");
		btnIntegreaza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				integreazaP1();
				Display();
			}
		});
		btnIntegreaza.setBounds(169, 61, 106, 23);
		contentPanel.add(btnIntegreaza);
		
	    btnIntegreaza_1 = new JButton("Integreaza 2");
		btnIntegreaza_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				integreazaP2();
				Display();
			}
		});
		btnIntegreaza_1.setBounds(169, 151, 106, 23);
		contentPanel.add(btnIntegreaza_1);
		
		this.textArea_4 = new JTextArea();
		textArea_4.setBounds(570, 120, 500, 20);
		textArea_4.setEditable(false); 
		contentPanel.add(textArea_4);
		textArea_4.setFont(fnt);
		
		
	}
	
	
	
	
	public void deriveazaP1(){
		this.P1.deriveaza();
	}
	
	public void deriveazaP2(){
		this.P2.deriveaza();
	}
	
	public void integreazaP1(){
		this.P1.integreaza();
		//this.textArea.setText(P1.integreaza().toString());
	}
	
	public void integreazaP2(){
		this.P2.integreaza();
	}
	
	public void addMonom1(){
		try{
		String nr = this.textField_0.getText().trim();
		this.textField_0.setText("");
		String nr1 = this.textField_1.getText().trim();
		this.textField_1.setText("");
		MonomInt m=new MonomInt(Integer.parseInt(nr),Integer.parseInt(nr1));
		this.P1.add(m);
		}
		catch (Exception e){
			JOptionPane.showMessageDialog(null,"Introduceţi gradul şi coeficientul!");
		}
		}
	
	public void addMonom2(){
		try{
			String nr = this.textField_2.getText().trim();
		
		this.textField_2.setText("");
		String nr1 = this.textField_3.getText().trim();
		this.textField_3.setText("");
		MonomInt m=new MonomInt(Integer.parseInt(nr),Integer.parseInt(nr1));
		this.P2.add(m);
		}
		catch (Exception e){
			JOptionPane.showMessageDialog(null,"Introduceţi gradul şi coeficientul!");
		}
	}
	
	public void aduna(){
		this.textArea_1.setText(this.P1.adunare(this.P2).toString());
	}
	
	public void scade(){
		this.textArea_2.setText(this.P1.scadere(this.P2).toString());
	}
	
	public void inmultire(){
		this.textArea_3.setText(this.P1.inmultire(this.P2).toString());
	}
	public void impartire(){
		if(this.P1.getList().get(0).getGrad()<this.P2.getList().get(0).getGrad())
		{
			btnPp.setEnabled(false);
			JOptionPane.showMessageDialog(null,"Nu se poate împărţi un polinom mai mic la unul mai mare!");
		}
		else{
			btnPp.setEnabled(true);
	     	this.textArea_4.setText(this.P1.impartire(this.P2).toString());
		}
	}
	
	public void Display()
	{
		String s1=this.P1.toString();
		this.textArea.setText(s1);
		
		String s2=this.P2.toString();
		this.textArea2.setText(s2);	
	}
}
