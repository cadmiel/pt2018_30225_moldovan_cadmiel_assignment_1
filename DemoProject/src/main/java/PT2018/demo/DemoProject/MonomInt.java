package PT2018.demo.DemoProject;

public class MonomInt extends Monom {
	
	private int coef;
	
	public MonomInt(int i,int g){
		this.grad=g;
		this.coef=i;
	}
	
	public MonomInt(){
		this.grad=0;
		this.coef=0;
	}
	
	@Override
	public Integer getCoef() {
		return this.coef;
	}
	
	public int getGrad(){
		return this.grad;
	}
	
	public boolean isInt(){
		return true;
	}
	
	@Override
	public Monom adunare(Monom M) {
		Monom m=new MonomInt(this.coef+M.getCoef().intValue(),this.grad);
		return m;
	}
	
	@Override
	public Monom scadere(Monom M) {
		Monom rez=new MonomInt(this.coef-M.getCoef().intValue(),this.grad);
		return rez;
	}
	
	public Monom integreaza(){
		int rezCoef=this.coef/(this.grad+1);
		int rezGrad=this.grad+1;
		Monom rez=new MonomInt(rezCoef,rezGrad);
		return rez;
	}
	
	public void deriveaza(){
		this.coef*=this.grad;
		this.grad-=1;
	}
	

	@Override
	public Monom auxImpartire(Monom M2) {
		int rest1=this.getCoef().intValue() / M2.getCoef().intValue();
		int restG=this.getGrad()-M2.getGrad();
		Monom m=new MonomInt(rest1,restG);
		return m;
	}

	@Override
	public Monom inmultire(Monom mon) {
		Monom auxM=new MonomInt(this.coef*mon.getCoef().intValue(),this.grad+mon.getGrad());
		return auxM;
	}
	
	public static String superscript(int s) {
		String str=""+s;
	    str = str.replaceAll("0", "⁰");
	    str = str.replaceAll("1", "¹");
	    str = str.replaceAll("2", "²");
	    str = str.replaceAll("3", "³");
	    str = str.replaceAll("4", "⁴");
	    str = str.replaceAll("5", "⁵");
	    str = str.replaceAll("6", "⁶");
	    str = str.replaceAll("7", "⁷");
	    str = str.replaceAll("8", "⁸");
	    str = str.replaceAll("9", "⁹");         
	    return str;
	}

	@Override
	public String toString() {
		String string="";
		if(coef==1){
			if (this.grad>0){
			string = "x"+superscript(this.getGrad());
			}
			else if(grad==0){
				string+="" +coef;
			}
			else{
				string="";
			}
					}
		else{
			if (this.grad>0){
		string = coef + "x"+superscript(this.getGrad());
			
		}
		else{
			string=""+coef;
		}
		}
		return string;
	}

	
}